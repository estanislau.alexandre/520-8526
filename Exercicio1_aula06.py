# 1) Crie uma classe que represente um ônibus. O ônibus deverá conter os seguintes atributos:

# capacidade total
# capacidade atual
# movimento

# Os comportamentos esperados para um Ônibus são:
# Embarcar
# Desembarcar
# Acelerar
# Frear

# Lembre-se que a capacidade total do ônibus é de 45 pessoas - não será possível admitir super-
# lotação. Além disso, quando o ônibus ficar vazio, não será permitido efetuar o desembarque
# de pessoas. Além disso, pessoas não podem embarcar ou desembarcar com o onibus em movimento.

class Onibus:
    def __init__(self, capacidade=45):
        self.capacidade = capacidade
        self.passageiros = 0
        self.velocidade = 0

    def embarcar(self, quantidade):
        if self.passageiros + quantidade <= self.capacidade:
            self.passageiros += quantidade
            print(f"{quantidade} passageiros embarcaram no ônibus.")
        else:
            print("Não há espaço suficiente para todos esses passageiros.")

    def desembarcar(self, quantidade):
        if self.passageiros - quantidade >= 0:
            self.passageiros -= quantidade
            print(f"{quantidade} passageiros desembarcaram do ônibus.")
        else:
            print("Não há passageiros suficientes no ônibus.")

    def acelerar(self, velocidade):
        self.velocidade += velocidade
        print(f"O ônibus acelerou para {self.velocidade} km/h.")

    def frear(self, velocidade):
        if self.velocidade - velocidade >= 0:
            self.velocidade -= velocidade
            print(f"O ônibus reduziu a velocidade para {self.velocidade} km/h.")
        else:
            print("O ônibus já está parado.")

# Exemplo de uso
if __name__ == "__main__":
    meu_onibus = Onibus()

    meu_onibus.embarcar(30)
    meu_onibus.desembarcar(10)
    meu_onibus.acelerar(50)
    meu_onibus.frear(20)