# Crie um script para simular uma lanchonete.
# 
# - Essa lanchonete deverá ter 5 tipos de comida e 3 tipos de bebidas no cardápio.
# - Quando clientes chegarem a essa lanchonete, eles deverão fazer um pedido aleatório de comida + bebida.
# - A lanchonete deverá ter um numero limitado de porções dessas comidas/bebidas. Comida = 5, bebida = 7.
# - Quando o cliente fazer o pedido, ele deverá ser notificado se as opções escolhidas estão disponiveis,
# e quais seus preços.

# Criar um DB.
# Criar uma tabela pra comida e uma pra bebida.
# Preencher essas tabelas.

# Criar uma função pra simular um cliente comprando algo.

import sqlite3
import random

# Função para inicializar o banco de dados
def inicializar_banco_dados():
    conexao = sqlite3.connect('lanchonete.db')
    cursor = conexao.cursor()

    # Criar tabela de comidas
    cursor.execute('''CREATE TABLE IF NOT EXISTS comidas (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        nome TEXT,
                        quantidade INTEGER,
                        preco FLOAT
                    )''')

    # Criar tabela de bebidas
    cursor.execute('''CREATE TABLE IF NOT EXISTS bebidas (
                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                        nome TEXT,
                        quantidade INTEGER,
                        preco FLOAT
                    )''')

    # Inserir dados iniciais
    cursor.execute("INSERT OR IGNORE INTO comidas (nome, quantidade, preco) VALUES ('Hamburguer', 5, 10)")
    cursor.execute("INSERT OR IGNORE INTO comidas (nome, quantidade, preco) VALUES ('Pizza', 5, 12)")
    cursor.execute("INSERT OR IGNORE INTO comidas (nome, quantidade, preco) VALUES ('Sanduíche', 5, 8)")
    cursor.execute("INSERT OR IGNORE INTO comidas (nome, quantidade, preco) VALUES ('Hot Dog', 5, 6)")
    cursor.execute("INSERT OR IGNORE INTO comidas (nome, quantidade, preco) VALUES ('Batata Frita', 5, 5)")

    cursor.execute("INSERT OR IGNORE INTO bebidas (nome, quantidade, preco) VALUES ('Refrigerante', 7, 3)")
    cursor.execute("INSERT OR IGNORE INTO bebidas (nome, quantidade, preco) VALUES ('Suco', 7, 4)")
    cursor.execute("INSERT OR IGNORE INTO bebidas (nome, quantidade, preco) VALUES ('Água', 7, 2)")

    conexao.commit()
    conexao.close()

# Função para fazer o pedido
def fazer_pedido():
    conexao = sqlite3.connect('lanchonete.db')
    cursor = conexao.cursor()

    comida_escolhida = random.choice(cursor.execute("SELECT nome FROM comidas WHERE quantidade > 0").fetchall())[0]
    bebida_escolhida = random.choice(cursor.execute("SELECT nome FROM bebidas WHERE quantidade > 0").fetchall())[0]

    preco_comida = cursor.execute("SELECT preco FROM comidas WHERE nome=?", (comida_escolhida,)).fetchone()[0]
    preco_bebida = cursor.execute("SELECT preco FROM bebidas WHERE nome=?", (bebida_escolhida,)).fetchone()[0]

    print("Pedido realizado com sucesso!")
    print("Comida: {} - Preço: R${}".format(comida_escolhida, preco_comida))
    print("Bebida: {} - Preço: R${}".format(bebida_escolhida, preco_bebida))

    cursor.execute("UPDATE comidas SET quantidade = quantidade - 1 WHERE nome=?", (comida_escolhida,))
    cursor.execute("UPDATE bebidas SET quantidade = quantidade - 1 WHERE nome=?", (bebida_escolhida,))

    conexao.commit()
    conexao.close()

if __name__ == "__main__":
    inicializar_banco_dados()

    for _ in range(5):  # Simula a chegada de 5 clientes
        print("\nCliente chegou à lanchonete.")
        fazer_pedido()