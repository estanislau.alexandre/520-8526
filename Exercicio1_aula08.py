# ====================================================================================
# 1. Escreva um programa utilizando funções que realize um cadastro.
# Deverão ser coletadas as seguintes informações:

# CPF
# Nome
# Idade
# Sexo
# Cidade

# Os registros deverão ser armazenados em um arquivo CSV.
# Para manter o padrão brasileiro, o CSV será separado pelo caractere ";".
# O programa deverá possuir uma função de consulta e de exclusão (POR NOME OU CPF).
# O programa deverá possuir tratamentos de erro, com a finalidade de que o programa nunca
# dê uma exceção e também que ele não aceite dados incorretos em nenhum momento.

import csv

def cadastrar_registro():
    while True:
        try:
            cpf = input("Digite o CPF (somente números): ").replace(".", "").replace("-", "").strip()
            if len(cpf) != 11 or not cpf.isdigit():
                raise ValueError("CPF inválido. O CPF deve conter 11 números.")
            
            nome = input("Digite o nome: ")
            idade = int(input("Digite a idade: "))
            sexo = input("Digite o sexo (M/F): ").upper()
            if sexo not in ['M', 'F']:
                raise ValueError("Sexo inválido. Digite M para masculino ou F para feminino.")
            
            cidade = input("Digite a cidade: ")

            with open("registros.csv", mode='a', newline='') as file:
                writer = csv.writer(file, delimiter=';')
                writer.writerow([cpf, nome, idade, sexo, cidade])
            
            print("Registro cadastrado com sucesso!")
            break
        except ValueError as ve:
            print(f"Erro: {ve}")

def consultar_registro(cpf):
    with open("registros.csv", mode='r', newline='') as file:
        reader = csv.reader(file, delimiter=';')
        for row in reader:
            if row[0] == cpf:
                print("Registro encontrado:")
                print(f"CPF: {row[0]}")
                print(f"Nome: {row[1]}")
                print(f"Idade: {row[2]}")
                print(f"Sexo: {row[3]}")
                print(f"Cidade: {row[4]}")
                return
        print("Registro não encontrado.")

def excluir_registro(cpf):
    registros = []
    with open("registros.csv", mode='r', newline='') as file:
        reader = csv.reader(file, delimiter=';')
        for row in reader:
            if row[0] != cpf:
                registros.append(row)
    
    with open("registros.csv", mode='w', newline='') as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerows(registros)
    
    print("Registro excluído com sucesso.")

# Teste das funções
if __name__ == "__main__":
    while True:
        print("\nMenu:")
        print("1. Cadastrar registro")
        print("2. Consultar registro por CPF")
        print("3. Excluir registro por CPF")
        print("4. Sair")
        opcao = input("Escolha uma opção: ")

        if opcao == "1":
            cadastrar_registro()
        elif opcao == "2":
            cpf_consulta = input("Digite o CPF para consulta: ")
            consultar_registro(cpf_consulta)
        elif opcao == "3":
            cpf_exclusao = input("Digite o CPF para exclusão: ")
            excluir_registro(cpf_exclusao)
        elif opcao == "4":
            print("Saindo...")
            break
        else:
            print("Opção inválida. Por favor, escolha uma opção válida.")