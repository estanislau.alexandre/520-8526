
# ===========================================================================================
# 2) Escreva um programa em python que conte as vogais que a música ‘Faroeste Caboclo’
# tem em sua letra. Armazena a letra da música em um arquivo do tipo txt.
# Dica: Não se esqueça de considerar as letras maiúsculas, minúsculas e com acentuação.

def contar_vogais(arquivo):
    vogais = 'aeiouáéíóúãẽĩõũâêîôûàèìòùäëïöü'
    contador = 0
    with open("faroeste.txt", 'r', encoding="UTF-8") as file:
        for linha in file:
            for caractere in linha:
                if caractere.lower() in vogais:
                    contador += 1
    return contador

nome_arquivo = 'faroeste.txt'
total_vogais = contar_vogais(nome_arquivo)
print(f"Total de vogais no arquivo '{nome_arquivo}': {total_vogais}")