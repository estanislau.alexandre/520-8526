import csv


def cadastrar_registro():
    while True:
        try:
            cpf = input("Digite o CPF (somente números): ")
            if len(cpf) != 11 or not cpf.isdigit():
                raise ValueError("CPF inválido. O CPF deve conter 11 números.")
            
            nome = input("Digite o nome: ")
            idade = int(input("Digite a idade: "))
            sexo = input("Digite o sexo (M/F): ").upper()
            if sexo not in ['M', 'F']:
                raise ValueError("Sexo inválido. Digite M para masculino ou F para feminino.")
            
            cidade = input("Digite a cidade: ")

            with open("registros.csv", mode='a', newline='') as file:
                writer = csv.writer(file, delimiter=';')
                writer.writerow([cpf, nome, idade, sexo, cidade])
            
            print("Registro cadastrado com sucesso!")
            break
        except ValueError as ve:
            print(f"Erro: {ve}")

cadastrar_registro()