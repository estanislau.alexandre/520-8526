# Exercício 2 aula 04
operacoes = {
    '1': lambda x,y: x+y,
    '2': lambda x,y: x-y,
    '3': lambda x,y: x/y,
    '4': lambda x,y: x*y,
    '5': lambda x,y: exit()
}


def calculadora():
    while True:
        print('-------------------------------')
        num1=float(input('Digite 1o Número: '))
        print('-------------------------------')
        num2=float(input('Digite 2o Número: '))
        print('-------------------------------')

        opcao = input(f'Operações Matemáticas:\n' \
                f'1 - Soma\n' \
                f'2 - Subtração\n' \
                f'3 - Divisão\n' \
                f'4 - Multiplicação\n'
                f'5 - Sair\n' \
                f'Qual operação matemática desejada?: ')


        if opcao in operacoes:
            print('-------------------------------')
            print(operacoes[opcao](num1,num2))
            print('-------------------------------')
        else:
            print('-------------------------------')
            print('Esta opção é inválida! Tente novamente')
            print('-------------------------------')


if __name__ == '__main__':
    calculadora()