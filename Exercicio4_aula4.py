# Exercício 4 aula 4
#cesta_frutas
cesta = []

frutas = {
  '1': 'Banana',
  '2': 'Melancia',
  '3': 'Morango'
}

precos = {
  'Banana' : 2.00,
  'Melancia' : 4.50,
  'Morango': 10.00

}

#menu_principal
while True:
    print('------------------------')
    print(f'Quitanda do Zé:\n' \
          f'1: Ver cesta\n' \
          f'2: Adicionar frutas\n' \
          f'3: Sair')

    op = input('Digite sua opção desejada: ')
    print('------------------------')

    if op == '1':
        print('Cesta de Frutas:')
        for item in cesta:
            print(item)
            print('------------------------')

    #menu_de_frutas
    elif op == '2':
        fruta = input(f'Menu de Frutas:\n' \
                      f'1 - Banana\n' \
                      f'2 - Melancia\n' \
                      f'3 - Morango\n' \
                      f'Digite sua opção desejada: ')
        print('------------------------')      
        if fruta in frutas:
            cesta.append(frutas[fruta])
            print(f'{frutas[fruta]} adicionado na sua Cesta de Frutas!')
            print('------------------------')

        else:
            print('Opção Inválida! Tente novamente.')
            print('------------------------')
    elif op == '3':
        if len(cesta) > 0:
            total = 0
            for item in cesta:
                total += precos[item]

            print('------------------------')
            print(f'Total de compras: {total}')
            print('------------------------')
            print(f'Cesta de compras: {cesta}')
            print('------------------------')
        else:
            print('------------------------')
            print('Cesta de compras vazia.')
            print('------------------------')

    elif op == '4':
        break

    else:
        print('------------------------')
        print('Opção Inválida')
        print('------------------------')