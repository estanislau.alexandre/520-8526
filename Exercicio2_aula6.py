# 2) Implemente um programa que represente uma fila. O contexto do programa é uma
# agência de banco. Cada cliente ao chegar deverá respeitar a seguinte regra: o primeiro
# a chegar deverá ser o primeiro a sair. Você poderá representar pessoas na fila a par-
# tir de números os quais representam a idade. A sua fila deverá conter os seguintes
# comportamentos:

# • Adicionar pessoa na fila: adicionar uma pessoa na fila.
# • Atender Fila: atender a pessoa respeitando a ordem de chegada
# • Dar prioridade: colocar uma pessoa maior de 65 anos como o primeiro da fila

class FilaBanco:
    def __init__(self):
        self.fila = []

    def adicionar_pessoa(self, idade):
        self.fila.append(idade)
        print(f"Pessoa com {idade} anos adicionada à fila.")

    def atender_fila(self):
        if self.fila:
            proximo_cliente = self.fila.pop(0)
            print(f"Atendendo cliente com {proximo_cliente} anos.")
        else:
            print("A fila está vazia. Não há clientes para atender.")

    def dar_prioridade(self, idade):
        self.fila.insert(0, idade)
        print(f"Cliente com {idade} anos recebeu prioridade e foi colocado no início da fila.")

# Exemplo de uso
if __name__ == "__main__":
    fila_banco = FilaBanco()

    # Adicionando pessoas na fila
    fila_banco.adicionar_pessoa(30)
    fila_banco.adicionar_pessoa(40)
    fila_banco.adicionar_pessoa(50)

    # Atendendo pessoas na fila
    fila_banco.atender_fila()
    fila_banco.atender_fila()

    # Dando prioridade para uma pessoa com mais de 65 anos
    fila_banco.dar_prioridade(70)

    # Atendendo novamente para verificar se a pessoa com prioridade foi atendida primeiro
    fila_banco.atender_fila()